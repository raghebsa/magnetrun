# First Prototype #
* Basic Gameplay Mechanic
* Basic gameobjects
* Simple textures and shaders
* Stage consists of two platforms one at the top and the other at the bottom
* Platforms can be of the same or opposite polarity

## [First Prototype Gameplay Footage](https://drive.google.com/open?id=0B5LZWhoBrbI4ZkVXYzhyZk5lRUE) ##


# Second Prototype #
* Added scoring mechanism
* Platforms can no longer be of the same polarity because the player looses control and results in gameover screen that cannot be avoided
* Refined the player movement
* Changed shaders and textures to a neon like theme

## [Second Prototype Gameplay Footage](https://drive.google.com/open?id=0B5LZWhoBrbI4enBiZ3BlVXlMX1U) ##
# Future plans: #
* Replace basic shapes with low-poly models
* Add power ups
* One of stage component from the sketch is missing (expermint with it)
* Add a visual cue for hazards' gravitational fields
* Add bonus points for Near Misses
* Title screen
* Run the game on a mobile device
* Read up on Unity ads
* Read up on publishing to the app store and google play