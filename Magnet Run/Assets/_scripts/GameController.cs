﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

	public float waitTime;
	public GameObject hazard;
	private float yValue;
	private bool gameOver;
	private float score;
	public Text scoreUi;

	void Start () {
		score = 0;
		gameOver = false;
		yValue = 4.5f;
		StartCoroutine (SpawnHazard ());
	}

	IEnumerator SpawnHazard()
	{
		while (!gameOver) {
			
			yield return new WaitForSeconds (waitTime);
			Vector3 spawnPosition = new Vector3 (transform.position.x,
										transform.position.y +Random.Range (-yValue, yValue),
				                   		transform.position.z);
			Quaternion spawnRotation = Quaternion.identity;
			Instantiate (hazard, spawnPosition, spawnRotation);
		}
	}

	public void GameOver()
	{
		gameOver = true;
	}

	void Update()
	{
		if (!gameOver) 
		{
			scoreUi.text = "Score: " + Mathf.FloorToInt(score).ToString();
		}
	}
	public void AddScore()
	{
		score += 10.0f;
	}
}
