﻿using UnityEngine;
using System.Collections;

public class ScoreCounter : MonoBehaviour {
	private GameObject gameObjectController;
	private GameController gameController;
	private Quaternion rotation;

	void Start()
	{
		gameObjectController = GameObject.FindGameObjectWithTag ("GameController");
		if (gameObjectController != null) {
			gameController = gameObjectController.GetComponent<GameController> ();
		} 
		else 
		{
			Debug.Log ("game controller couldn't be initialized");
		}
		rotation = transform.rotation;
	}

	void LateUpdate()
	{
		transform.rotation = rotation;
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			gameController.AddScore ();
		}
	}
}
