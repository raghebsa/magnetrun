﻿using UnityEngine;
using System.Collections;

public class CameraTracker : MonoBehaviour {

	public Transform playerTransform;
	private float xOffset;
	private Vector3 newPosition;
	// Use this for initialization
	void Start () {
		xOffset = transform.position.x - playerTransform.position.x;
		newPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (playerTransform != null) 
		{
			newPosition.x = playerTransform.position.x + xOffset;
			transform.position = newPosition;
		}
	}
}
