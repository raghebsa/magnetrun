﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

	private bool isSouth;
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			Destroy (other.gameObject);
		}
	}
}
