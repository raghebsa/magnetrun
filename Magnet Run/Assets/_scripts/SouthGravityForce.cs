﻿using UnityEngine;
using System.Collections;

public class SouthGravityForce : MonoBehaviour {

	public float speed;

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) {
			if (other.GetComponent<PlayerController> ().IsSouth ()) {
				SetupReaction (other, speed);
			} else {
				SetupReaction (other, -speed);
			}
		}
	}

	void SetupReaction(Collider other, float speed)
	{
		if (other.transform.position.y - transform.position.y < 0) {
			other.gameObject.GetComponent<Rigidbody> ().velocity = (transform.up * -speed) + (transform.right * other.GetComponent<PlayerController> ().horizontalSpeed);
		} else {
			other.gameObject.GetComponent<Rigidbody> ().velocity = (transform.up * speed) + (transform.right * other.GetComponent<PlayerController> ().horizontalSpeed);
		}
	}
}
