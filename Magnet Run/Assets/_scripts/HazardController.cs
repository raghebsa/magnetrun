﻿using UnityEngine;
using System.Collections;

public class HazardController : MonoBehaviour {

	public float tumble;
	private float speed;
	private bool isSouth;
	public Material south;
	public Material north;

	void Start () {
		speed = 0;
		GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
		isSouth = Random.Range (0, 2) == 1;
		if (isSouth) 
			GetComponent<Renderer> ().sharedMaterial = south;
		else
			GetComponent<Renderer> ().sharedMaterial = north;
	}
	
	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			speed = (other.GetComponent<PlayerController> ().horizontalSpeed*0.70f) * Time.deltaTime;
			if(other.GetComponent<PlayerController> ().IsSouth() == isSouth)
				transform.position = Vector3.MoveTowards (transform.position, other.gameObject.transform.position, -speed);
			else
				transform.position = Vector3.MoveTowards (transform.position, other.gameObject.transform.position, speed);
		}
	}
}
