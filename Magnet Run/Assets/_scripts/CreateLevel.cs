﻿using UnityEngine;
using System.Collections;

public class CreateLevel : MonoBehaviour {
	public GameObject[] level;
	private int triggerCalls;
	private Vector3 nextPosition;
	// Use this for initialization
	void Start () {
		SpawnPlatform ();
	}

	void SpawnPlatform()
	{
		nextPosition = transform.position;
		Instantiate (level [Random.Range (0, level.Length)],nextPosition, new Quaternion());
	}

	void SpawnNextPlatform()
	{
		nextPosition = new Vector3(transform.position.x + 40f, transform.position.y, transform.position.z);
		Instantiate (level [Random.Range (0, level.Length)],nextPosition, new Quaternion());
		UpdateNextPosition ();
	}

	void UpdateNextPosition()
	{
		transform.position = new Vector3 (transform.position.x + 40f, transform.position.y, transform.position.z);
	}
		
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) {
			SpawnNextPlatform ();
		}
	}
}
