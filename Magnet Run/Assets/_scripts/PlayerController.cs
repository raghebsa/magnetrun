﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public Material north;
	public Material south;

	public float horizontalSpeed;
	public float vertSpeed;
	public float switchRate;
	private bool isSouth;
	public GameObject gameController;

	void Start () 
	{
		vertSpeed = 0.0f;
		GetComponent<Rigidbody> ().velocity = transform.right * horizontalSpeed;
		isSouth = false;
	}

	public bool IsSouth()
	{
		return isSouth;
	}

	void Update()
	{
		if (Input.GetButtonUp("Fire1"))
		{
			isSouth = !isSouth;
			SwitchMaterial();
		}
	}

//	void FixedUpdate()
//	{
//		speed += 0.0025f;
//	}

	void SwitchMaterial()
	{
		if (isSouth)
			GetComponent<Renderer> ().sharedMaterial = south;
		else
			GetComponent<Renderer> ().sharedMaterial = north;
	}

	void OnDestroy()
	{
		gameController.GetComponent<GameController> ().GameOver ();
	}
}
