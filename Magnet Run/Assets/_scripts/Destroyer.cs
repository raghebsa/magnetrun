﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	void OnTriggerExit(Collider other)
	{
		if (!other.gameObject.CompareTag ("Score Counter")) 
		{
			Destroy (other.gameObject);
		}
	}
}
